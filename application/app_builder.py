from functools import wraps, update_wrapper
from flask import Flask, render_template, request, session, redirect, url_for, current_app
from flask_pymongo import PyMongo
from flask_login import LoginManager, current_user
from flask_mail import Mail
from datetime import datetime
from flask_login.config import EXEMPT_METHODS

app = Flask(__name__)
app.config.from_object('conf')
app.config['MAX_CONTENT_LENGTH'] = 20 * 1024 * 1024
app.secret_key = 'Jaguar2019!@#'
mongo = PyMongo(app)
login_manager = LoginManager(app)
mail = Mail(app)

def humanize_ts(timestamp=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    now = datetime.now()
    diff = now - timestamp
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(int(second_diff)) + " seconds ago"
        if second_diff < 120:
            return "a minute ago"
        if second_diff < 3600:
            return str(int(second_diff / 60)) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str(int(second_diff / 3600)) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(int(day_diff / 7)) + " weeks ago"
    if day_diff < 365:
        return str(int(day_diff / 30)) + " months ago"
    return str(int(day_diff / 365)) + " years ago"

app.jinja_env.filters['humanize'] = humanize_ts


def role_required(role):
    def decorator(func):
        def wrapped_function(*args, **kwargs):
            if request.method in EXEMPT_METHODS:
                return func(*args, **kwargs)
            elif current_app.login_manager._login_disabled:
                return func(*args, **kwargs)
            elif not current_user.is_authenticated:
                return current_app.login_manager.unauthorized()
            elif current_user.role not in role:
                if "contributor" in role:
                    if not current_user.is_contrib:
                        return current_app.login_manager.unauthorized()
            return func(*args, **kwargs)
        return update_wrapper(wrapped_function, func)
    return decorator



@app.errorhandler(404)
def not_found_error(error):
    return render_template('error_handler/404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    return render_template('error_handler/500.html'), 500

@app.errorhandler(405)
def method_not_allowed(error):
    return render_template('error_handler/405.html'), 405

@app.errorhandler(502)
def bad_gateway(error):
    return render_template('error_handler/502.html'), 502

@app.before_request
def check_for_maintenance():
    if app.config["MAINTENANCE_MODE"]:
        if request.endpoint == 'static':
            pass
        else:
            return render_template('maintenance/index.html')