import os, glob, re, pymongo, html_sanitizer
from html.parser import HTMLParser
from math import ceil
from bson.objectid import ObjectId
from application import app, mongo
from application.helpers import forms
from flask import Blueprint, render_template, redirect, url_for, request, flash, jsonify
from flask_login import current_user
from datetime import datetime

lumbungilmu = Blueprint('lumbungilmu', __name__, url_prefix='/lumbungilmu')

@lumbungilmu.route('/', methods=["GET","POST"])
def index():
    if request.method == "GET":
        for x in glob.glob('application/static/img/headers/lumbungilmu.*'):
            header_image = url_for('static', filename=x)
        header_image = header_image.split('/')[-1:][0]
        header_image = url_for('static', filename="img/headers/"+header_image)
        article = mongo.db.articles.find({"is_active":True,"category":{'$exists':True}}).sort("created_at",pymongo.DESCENDING)
        newes = mongo.db.articles.find({"is_active":True,"category":{'$exists':True}}).sort("created_at", pymongo.DESCENDING).limit(3)
        newest = []
        for x in newes:
            x["thumbnail"] = url_for('static', filename="img/articles/" + x["thumbnail"])
            x["content"] = re.compile(r'<[^>]+>').sub('', x["content"])
            newest.append(x)
        pipeline = [{"$sort":{"count":1}},{"$limit":5},{"$group": {"_id": "$article", "count": {"$sum": 1}}}]
        pop = mongo.db.statistics.aggregate(pipeline)
        popular = []
        for x in pop:
            pop_art = mongo.db.articles.find_one({"_id": ObjectId(x["_id"])})
            try:
                pop_art["category"]
            except KeyError:
                pass
            else:
                pop_art["thumbnail"] = url_for('static', filename="img/articles/" + pop_art["thumbnail"])
                pop_art["content"] = re.compile(r'<[^>]+>').sub('', pop_art["content"])
                popular.append(pop_art)
        article = mongo.db.articles.find({"is_active":True,"category":{'$exists':True}}).sort("created_at",pymongo.DESCENDING).limit(4)
        articles = []
        for x in article:
            category = mongo.db.categories.find_one({"_id": ObjectId(x["category"])})
            x["category_name"] = category["category"]
            x["thumbnail"] = url_for('static', filename="img/articles/"+x["thumbnail"])
            x["content"] = re.compile(r'<[^>]+>').sub('',x["content"])
            articles.append(x)
        count = article.count()
        return render_template('lumbungilmu.html', newest=newest, popular=popular, header_image=header_image, articles=articles, current_page=1, pages=ceil(count/4))
    else:
        for x in glob.glob('application/static/img/headers/lumbungilmu.*'):
            header_image = url_for('static', filename=x)
        header_image = header_image.split('/')[-1:][0]
        header_image = url_for('static', filename="img/headers/"+header_image)
        search = str(request.form["search"])

        rgx = re.compile('.*'+search+'.*', re.IGNORECASE)
        article = mongo.db.articles.find({"is_active": True, "category": {'$exists': True},"judul":rgx}).sort("created_at",pymongo.DESCENDING)
        newes = mongo.db.articles.find({"is_active": True, "category": {'$exists': True}}).sort("created_at", pymongo.DESCENDING).limit(3)
        newest = []
        for x in newes:
            x["thumbnail"] = url_for('static', filename="img/articles/" + x["thumbnail"])
            x["content"] = re.compile(r'<[^>]+>').sub('', x["content"])
            newest.append(x)
        pipeline = [{"$sort":{"count":1}},{"$limit":5},{"$group": {"_id": "$article", "count": {"$sum": 1}}}]
        pop = mongo.db.statistics.aggregate(pipeline)
        popular = []
        for x in pop:
            pop_art = mongo.db.articles.find_one({"_id": ObjectId(x["_id"])})
            try:
                pop_art["category"]
            except KeyError:
                pass
            else:
                pop_art["thumbnail"] = url_for('static', filename="img/articles/" + pop_art["thumbnail"])
                pop_art["content"] = re.compile(r'<[^>]+>').sub('', pop_art["content"])
                popular.append(pop_art)
        article = mongo.db.articles.find({"is_active": True, "category": {'$exists': True},"judul":rgx}).sort("created_at",pymongo.DESCENDING).limit(4)
        articles = []
        for x in article:
            category = mongo.db.categories.find_one({"_id": ObjectId(x["category"])})
            x["category_name"] = category["category"]
            x["thumbnail"] = url_for('static', filename="img/articles/"+x["thumbnail"])
            x["content"] = re.compile(r'<[^>]+>').sub('',x["content"])
            articles.append(x)
        count = article.count()
        return render_template('lumbungilmu.html', newest=newest, popular=popular, keyword=search, header_image=header_image, articles=articles, current_page=1, pages=ceil(count/4))

@lumbungilmu.route('/<int:page>', methods=["GET"], defaults={"keyword":""})
@lumbungilmu.route('/<string:keyword>/<int:page>', methods=["GET"])
def search(keyword,page):
    for x in glob.glob('application/static/img/headers/lumbungilmu.*'):
        header_image = url_for('static', filename=x)
    header_image = header_image.split('/')[-1:][0]
    header_image = url_for('static', filename="img/headers/" + header_image)
    search = keyword
    rgx = re.compile('.*' + search + '.*', re.IGNORECASE)
    article = mongo.db.articles.find({"is_active": True, "judul": rgx}).sort("created_at",pymongo.DESCENDING)
    newes = mongo.db.articles.find({"is_active": True, "category": {'$exists': True}}).sort("created_at",
                                                                                            pymongo.DESCENDING).limit(3)
    newest = []
    for x in newes:
        print(x["judul"])
        x["thumbnail"] = url_for('static', filename="img/articles/" + x["thumbnail"])
        x["content"] = re.compile(r'<[^>]+>').sub('', x["content"])
        newest.append(x)
    pipeline = [{"$sort":{"count":1}},{"$limit":5},{"$group": {"_id": "$article", "count": {"$sum": 1}}}]
    pop = mongo.db.statistics.aggregate(pipeline)
    popular = []
    for x in pop:
        pop_art = mongo.db.articles.find_one({"_id": ObjectId(x["_id"])})
        pop_art["thumbnail"] = url_for('static', filename="img/articles/" + pop_art["thumbnail"])
        pop_art["content"] = re.compile(r'<[^>]+>').sub('', pop_art["content"])
        popular.append(pop_art)
    for x in newest:
        x["thumbnail"] = url_for('static', filename="img/articles/"+x["thumbnail"])
        x["content"] = re.compile(r'<[^>]+>').sub('', x["content"])
    if page > 1:
        # article = mongo.db.articles.find({"is_active": True, "judul": rgx}).sort("created_at",pymongo.DESCENDING).skip(4*(page-1)).limit(4)
        article = mongo.db.articles.find({"is_active": True, "category": {'$exists': True}, "judul": rgx}).sort(
            "created_at", pymongo.DESCENDING).skip(4*(page-1)).limit(4)
    else:
        article = mongo.db.articles.find({"is_active": True, "category": {'$exists': True}, "judul": rgx}).sort(
            "created_at", pymongo.DESCENDING).limit(4)
    articles = []
    for x in article:
        category = mongo.db.categories.find_one({"_id": ObjectId(x["category"])})
        x["category_name"] = category["category"]
        x["thumbnail"] = url_for('static', filename="img/articles/" + x["thumbnail"])
        x["content"] = re.compile(r'<[^>]+>').sub('', x["content"])
        articles.append(x)
    count = article.count()
    return render_template('lumbungilmu.html', keyword=search, header_image=header_image, articles=articles,
                           current_page=page, pages=ceil(count / 4), newest=newest, popular=popular)

@lumbungilmu.route('/detail/<string:slug>', methods=["GET"])
def detail(slug):
    article = mongo.db.articles.find_one({"is_active":True,"slug":slug})
    counter = mongo.db.statistics.insert_one({"article":ObjectId(article["_id"]),"viewed_at":datetime.now()})
    newes = mongo.db.articles.find({"is_active": True, "category": {'$exists': True}}).sort("created_at",
                                                                                            pymongo.DESCENDING).limit(3)
    newest = []
    for x in newes:
        x["thumbnail"] = url_for('static', filename="img/articles/" + x["thumbnail"])
        x["content"] = re.compile(r'<[^>]+>').sub('', x["content"])
        newest.append(x)
    pipeline = [{"$sort": {"count": 1}},{"$limit":5}, {"$group": {"_id": "$article", "count": {"$sum": 1}}}]
    pop = mongo.db.statistics.aggregate(pipeline)
    popular = []
    for x in pop:
        pop_art = mongo.db.articles.find_one({"_id": ObjectId(x["_id"])})
        try:
            pop_art["category"]
        except KeyError:
            pass
        else:
            pop_art["thumbnail"] = url_for('static', filename="img/articles/" + pop_art["thumbnail"])
            pop_art["content"] = re.compile(r'<[^>]+>').sub('', pop_art["content"])
            popular.append(pop_art)
    try:
        article["category"]
    except:
        article["category_name"] = "Gugur Gunung"
    else:
        category = mongo.db.categories.find_one({"_id": ObjectId(article["category"])})
        article["category_name"] = category["category"]
    article["thumbnail"] = url_for('static', filename="img/articles/" + article["thumbnail"])
    if os.path.isfile("application"+url_for('static', filename="infographics/"+article["slug"]+".pdf")):
        article["infographic"] = url_for('static', filename="infographics/"+article["slug"]+".pdf")
    else:
        article["infographic"] = None
    author = mongo.db.users.find_one({"_id":ObjectId(article["author"])})
    if author["role"] != "admin":
        author = mongo.db.visitors.find_one({"_id":ObjectId(author["visitor_id"])})
        author["avatar_url"] = url_for('static', filename="img/avatar/" + author["avatar_url"])
    else:
        author["fullname"] = "Admin"
        author["avatar_url"] = url_for('static', filename="img/notfound.png")
    comments = mongo.db.comments.find({"article":article["_id"]}).sort("timestamp",pymongo.ASCENDING)
    comments_data = []
    for x in comments:
        replieses = []
        commentator = mongo.db.users.find_one({"_id":x["commenter"]})
        if commentator:
            if commentator["role"] != "admin":
                commentator = mongo.db.visitors.find_one({"_id": ObjectId(commentator["visitor_id"])})
                if commentator["fullname"] == "" or None:
                    commentator["fullname"] = "Anonim"
                commentator["avatar_url"] = url_for('static', filename="img/avatar/" + commentator["avatar_url"])
            else:
                commentator["fullname"] = "Admin"
                commentator["avatar_url"] = url_for('static', filename="img/notfound.png")
        else:
            commentator = {}
            commentator["fullname"] = "Anonim"
            commentator["avatar_url"] = url_for('static', filename="img/notfound.png")
        x["commenter"] = commentator

        replies = mongo.db.replies.find({"comment_id": ObjectId(x["_id"])}).sort("timestamp",pymongo.ASCENDING)
        for j in replies:
            replier = mongo.db.users.find_one({"_id":j["replier"]})
            if replier["role"] != "admin":
                replier = mongo.db.visitors.find_one({"_id": ObjectId(replier["visitor_id"])})
                if replier["fullname"] == "" or None:
                    replier["fullname"] = "Anonim"
                replier["avatar_url"] = url_for('static', filename="img/avatar/" + replier["avatar_url"])
            else:
                replier["fullname"] = "Admin"
                replier["avatar_url"] = url_for('static', filename="img/notfound.png")
            j["replier"] = replier
            replieses.append(j)
        x["replies"] = replieses
        comments_data.append(x)
    print(comments_data)
    return render_template('detail.html', article=article, author=author, newest=newest, popular=popular, comments_data=comments_data)

@lumbungilmu.route('/comment/<string:id>', methods=["POST"])
def comment(id):
    if ObjectId.is_valid(id):
        article = mongo.db.articles.find_one({"_id":ObjectId(id)})
        if not article:
            flash({'New Comment': 'Article not found.'}, 'danger')
            return redirect(url_for('lumbungilmu.index'))
        form = forms.CommentForm()
        if form.validate_on_submit():
            user = current_user.__dict__
            user_detail = mongo.db.users.find_one({"username": user["username"]})
            comment = mongo.db.comments.insert_one({"commenter":ObjectId(user_detail["_id"]),"comment":form.comment.data,"article":ObjectId(id),"timestamp":datetime.now()})
            return redirect(url_for('lumbungilmu.detail',slug=article["slug"], _anchor='comments'))
        else:
            for x, y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors, 'danger')
            return redirect(url_for('lumbungilmu.detail',slug=article["slug"]))
    else:
        flash({'New Comment': 'Article not found!'}, 'danger')
        return redirect(url_for('lumbungilmu.index'))

@lumbungilmu.route('/reply/<string:id>', methods=["POST"])
def reply(id):
    if ObjectId.is_valid(id):
        comment = mongo.db.comments.find_one({"_id":ObjectId(id)})
        if not comment:
            flash({'New Comment': 'Article not found.'}, 'danger')
            return redirect(url_for('lumbungilmu.index'))
        article = mongo.db.articles.find_one({"_id":comment["article"]})
        form = forms.CommentForm()
        if form.validate_on_submit():
            user = current_user.__dict__
            user_detail = mongo.db.users.find_one({"username": user["username"]})
            comment = mongo.db.replies.insert_one({"comment_id": ObjectId(comment["_id"]), "replier": user_detail["_id"], "comment": form.comment.data, "timestamp": datetime.now()})
            return redirect(url_for('lumbungilmu.detail',slug=article["slug"],_anchor='comments'))
        else:
            for x, y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors, 'danger')
            return redirect(url_for('lumbungilmu.detail',slug=article["slug"]))
    else:
        flash({'New Reply': 'Comment not found to reply!'}, 'danger')
        return redirect(url_for('lumbungilmu.index'))