from application import app
from application.modules.home.routes import home
from application.modules.berasmerah.routes import berasmerah
from application.modules.lumbungilmu.routes import lumbungilmu
from application.modules.pedati.routes import pedati
from application.modules.gugurgunung.routes import gugurgunung
from application.modules.admin.home.routes import admin_home
from application.modules.admin.berasmerah.routes import admin_berasmerah
from application.modules.admin.lumbungilmu.routes import admin_lumbungilmu
from application.modules.admin.pedati.routes import admin_pedati
from application.modules.admin.profile.routes import admin_profile
from application.modules.admin.pengguna.routes import admin_pengguna
from application.modules.admin.welcome.routes import admin_welcome

app.register_blueprint(home)
app.register_blueprint(berasmerah)
app.register_blueprint(lumbungilmu)
app.register_blueprint(pedati)
app.register_blueprint(gugurgunung)
app.register_blueprint(admin_home)
app.register_blueprint(admin_berasmerah)
app.register_blueprint(admin_lumbungilmu)
app.register_blueprint(admin_pedati)
app.register_blueprint(admin_profile)
app.register_blueprint(admin_pengguna)
app.register_blueprint(admin_welcome)