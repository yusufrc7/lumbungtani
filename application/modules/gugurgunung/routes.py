import os, glob, re, pymongo
from html.parser import HTMLParser
from math import ceil
from bson.objectid import ObjectId
from application import app, mongo
from application.helpers import forms
from flask import Blueprint, render_template, redirect, url_for, request, flash, jsonify
from flask_login import current_user
from datetime import datetime

gugurgunung = Blueprint('gugurgunung', __name__, url_prefix='/gugurgunung')


@gugurgunung.route('/', methods=["GET","POST"])
def index():
    if request.method == "GET":
        for x in glob.glob('application/static/img/headers/lumbungilmu.*'):
            header_image = url_for('static', filename=x)
        header_image = header_image.split('/')[-1:][0]
        header_image = url_for('static', filename="img/headers/"+header_image)
        article = mongo.db.articles.find({"is_active":True,"category":{'$exists':False}}).sort("created_at",pymongo.DESCENDING)
        count = article.count()
        article = mongo.db.articles.find({"is_active":True,"category":{'$exists':False}}).sort("created_at",pymongo.DESCENDING).limit(4)
        articles = []
        for x in article:
            x["thumbnail"] = url_for('static', filename="img/articles/"+x["thumbnail"])
            x["content"] = re.compile(r'<[^>]+>').sub('',x["content"])
            articles.append(x)
        return render_template('gugurgunung.html', header_image=header_image, articles=articles, current_page=1, pages=ceil(count/4))
    else:
        for x in glob.glob('application/static/img/headers/lumbungilmu.*'):
            header_image = url_for('static', filename=x)
        header_image = header_image.split('/')[-1:][0]
        header_image = url_for('static', filename="img/headers/"+header_image)
        search = str(request.form["search"])

        rgx = re.compile('.*'+search+'.*', re.IGNORECASE)
        article = mongo.db.articles.find({"is_active": True, "category": {'$exists': False},"judul":rgx}).sort("created_at",pymongo.DESCENDING)
        count = article.count()
        article = mongo.db.articles.find({"is_active": True, "category": {'$exists': False},"judul":rgx}).sort("created_at",pymongo.DESCENDING).limit(4)
        articles = []
        for x in article:
            x["thumbnail"] = url_for('static', filename="img/articles/"+x["thumbnail"])
            x["content"] = re.compile(r'<[^>]+>').sub('',x["content"])
            articles.append(x)
        return render_template('gugurgunung.html', keyword=search, header_image=header_image, articles=articles, current_page=1, pages=ceil(count/4))