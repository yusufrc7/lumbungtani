import os
from bson.objectid import ObjectId
from application import app, mongo, role_required
from application.helpers import forms
from flask import Blueprint, render_template, url_for, request, redirect, flash
from flask_login import current_user
from werkzeug import secure_filename

admin_profile = Blueprint('admin_profile', __name__, url_prefix='/admin/profile')

@admin_profile.route('/edit', methods=["GET","POST"])
@role_required(["admin","visitor"])
def index():
    if request.method == "POST":
        form = forms.EditProfileForm()
        if form.validate_on_submit():
            profile = mongo.db.users.find_one({"_id":ObjectId(current_user.id)})
            detail = mongo.db.visitors.find_one({"_id":ObjectId(profile["visitor_id"])})
            newdata = {}
            if form.avatar.data is not None:
                filename = secure_filename(form.avatar.data.filename)
                ext = filename.rsplit('.', 1)[1].lower()
                basefilename = secure_filename(str(current_user.id))
                os.remove('application'+current_user.avatar_url)
                form.avatar.data.save('application/static/img/avatar/'+str(basefilename)+'.'+ext)
                newdata["avatar_url"] = str(basefilename)+'.'+ext
            newdata["fullname"] = form.name.data
            newdata["phone"] = form.phone.data
            newdata["facebook"] = form.facebook.data
            newdata["twitter"] = form.twitter.data
            newdata["instagram"] = form.instagram.data
            newdata["linkedin"] = form.linkedin.data
            newdata["bio"] = form.bio.data
            mongo.db.visitors.update_one({"_id":ObjectId(detail["_id"])},{'$set':newdata})
            flash({'Change Data': 'Change Data Successfull!'}, 'success')
            return redirect(url_for('admin_profile.index'))
        else:
            for x, y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors, 'danger')
            return redirect(url_for('admin_profile.index'))
    else:
        return render_template('admin/profile/edit.html')