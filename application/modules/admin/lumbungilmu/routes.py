import os, glob, pymongo
from bson.objectid import ObjectId
from application import app, mongo, role_required
from flask import Blueprint, render_template, url_for, request, flash, redirect, current_app
from application.helpers import forms
from werkzeug import secure_filename
from datetime import datetime
from flask_login import current_user
import html_sanitizer

admin_lumbungilmu = Blueprint('admin_lumbungilmu', __name__, url_prefix='/admin/lumbungilmu')

@admin_lumbungilmu.route('/', methods=["GET"])
@role_required(["admin","contributor"])
def index():
    if current_user.role != 'admin':
        article = mongo.db.articles.find({"author":ObjectId(current_user.id)}).sort("created_at", pymongo.DESCENDING)
    else:
        article = mongo.db.articles.find({}).sort("created_at", pymongo.DESCENDING)
    articles = []
    for x in article:
        try:
            x["category"]
        except:
            x["category_name"] = "Gugur Gunung"
        else:
            category = mongo.db.categories.find_one({"_id": ObjectId(x["category"])})
            x["category_name"] = category["category"]
        articles.append(x)
    return render_template('admin/lumbungilmu.html', articles=articles)

@admin_lumbungilmu.route('/create', methods=["GET","POST"])
@role_required(["admin"])
def create():
    if request.method == "POST":
        form = forms.ArticleForm()
        if form.validate_on_submit():
            if ObjectId.is_valid(form.category.data) or form.category.data == "":
                if form.category.data != "":
                    category = mongo.db.category.find({"_id":ObjectId(form.category.data)})
                    if not category:
                        flash({'New Article': 'Article creation failed! Category unknown.'}, 'danger')
                        categories = mongo.db.category.find({})
                        return render_template('admin/lumbungilmu/create.html', categories=categories)
                slug = str(form.judul.data).lower().replace(" ","-")
                counter = 0
                exist = True
                while exist:
                    check = mongo.db.articles.find_one({"slug": slug+str(counter)})
                    if check:
                        exist = True
                        counter+=1
                    else:
                        exist = False
                slug = slug+str(counter)
                filename = secure_filename(form.thumbnail.data.filename)
                infographic = secure_filename(form.infographic.data.filename)
                ext = filename.rsplit('.', 1)[1].lower()
                infographic_ext = infographic.rsplit('.', 1)[1].lower()
                form.thumbnail.data.save('application/static/img/articles/' + slug + '.' + ext)
                form.infographic.data.save('application/static/infographics/' + slug + '.' + infographic_ext)
                user = current_user.__dict__
                author_detail = mongo.db.users.find_one({"username":user["username"]})
                # Make a copy
                my_settings = dict(html_sanitizer.sanitizer.DEFAULT_SETTINGS)
                # Add your changes
                my_settings['tags'].add('img')
                my_settings['tags'].add('table')
                my_settings['tags'].add('tr')
                my_settings['tags'].add('td')
                my_settings['tags'].add('th')
                my_settings['empty'].add('img')
                my_settings['attributes'].update({'img': ('src',)})
                sanitizer = html_sanitizer.Sanitizer(settings=my_settings)
                content = sanitizer.sanitize(form.content.data)
                print(content.encode('utf-8'))
                if form.category.data == "":
                    article = mongo.db.articles.insert_one({"created_at": datetime.now(), "thumbnail": slug + '.' + ext, "slug": slug, "judul": form.judul.data, "is_active": form.status.data.lower() in ('true'), "content": content.replace("B iosekuriti", "Biosekuriti").replace("d alam", "dalam"), "author": author_detail["_id"]})
                else:
                    article = mongo.db.articles.insert_one({"created_at":datetime.now(),"thumbnail":slug + '.' + ext,"slug":slug,"judul":form.judul.data,"category":ObjectId(form.category.data),"is_active":form.status.data.lower() in ('true'),"content":content.replace("B iosekuriti", "Biosekuriti").replace("d alam", "dalam"),"author":author_detail["_id"]})
                flash({'New Article': 'Article creation success!'}, 'success')
                return redirect(url_for('admin_lumbungilmu.index'))
            else:
                flash({'New Article': 'Article creation failed! Please contact admin.'}, 'danger')
                categories = mongo.db.category.find()
                return render_template('admin/lumbungilmu/create.html', categories=categories)
        else:
            for x, y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors, 'danger')
            categories = mongo.db.category.find()
            return render_template('admin/lumbungilmu/create.html', categories=categories)
    else:
        categories = mongo.db.categories.find()
        return render_template('admin/lumbungilmu/create.html', categories=categories)

@admin_lumbungilmu.route('/edit/<string:id>', methods=["GET","POST"])
@role_required(["admin"])
def edit(id):
    if ObjectId.is_valid(id):
        if request.method == "GET":
            article = mongo.db.articles.find_one({"_id": ObjectId(id)})
            if article:
                try:
                    article["category"]
                except KeyError:
                    article["category"] = ""
                    article["category_name"] = "Pilih Kategori"
                else:
                    category = mongo.db.categories.find_one({"_id": ObjectId(article["category"])})
                    article["category_name"] = category["category"]
                categories = mongo.db.categories.find({"_id":{"$ne":article["category"]}})
                return render_template('admin/lumbungilmu/edit.html', article=article, categories=categories)
            else:
                flash({'Edit Article': 'Article not found!'}, 'danger')
                return redirect(url_for('admin_lumbungilmu.index'))
        else:
            form = forms.ArticleEditForm()
            if form.validate_on_submit():
                article = mongo.db.articles.find_one({"_id": ObjectId(id)})
                if ObjectId.is_valid(form.category.data):
                    category = mongo.db.category.find({"_id":ObjectId(form.category.data)})
                    if not category:
                        flash({'Edit Article': 'Article edit failed! Category unknown.'}, 'danger')
                        categories = mongo.db.category.find({})
                        return render_template('admin/lumbungilmu/create.html', categories=categories)
                    slug = str(form.judul.data).lower().replace(" ", "-")
                    counter = 0
                    exist = True
                    while exist:
                        check = mongo.db.articles.find_one({"slug": slug + str(counter)})
                        if check:
                            exist = True
                            counter += 1
                        else:
                            exist = False
                    slug = slug + str(counter)
                    thumbnail = article["thumbnail"]
                    if form.thumbnail.data is not None:
                        filename = secure_filename(form.thumbnail.data.filename)
                        ext = filename.rsplit('.', 1)[1].lower()
                        os.remove('application/static/img/articles/' + article["thumbnail"])
                        form.thumbnail.data.save('application/static/img/articles/' + slug + '.' + ext)
                        thumbnail = slug + '.' + ext
                    # Make a copy
                    my_settings = dict(html_sanitizer.sanitizer.DEFAULT_SETTINGS)
                    # Add your changes
                    my_settings['tags'].add('img')
                    my_settings['tags'].add('table')
                    my_settings['tags'].add('tbody')
                    my_settings['tags'].add('tr')
                    my_settings['tags'].add('td')
                    my_settings['tags'].add('th')
                    my_settings['tags'].add('caption')
                    my_settings['tags'].add('span')
                    my_settings['empty'].add('img')
                    my_settings['empty'].add('td')
                    my_settings['empty'].add('th')
                    my_settings['attributes'].update({'span': ('class','style','valign','lang')})
                    my_settings['attributes'].update({'p': ('style')})
                    my_settings['attributes'].update({'td': ('class','rowspan','colspan','width','style', 'align', 'valign', 'nowrap')})
                    my_settings['attributes'].update({'tr': ('class','rowspan','colspan','width','style', 'align', 'valign', 'nowrap')})
                    my_settings['attributes'].update({'th': ('class','rowspan','colspan','width','style', 'align', 'valign', 'nowrap')})
                    my_settings['attributes'].update({'table': ('class','align','style','width','cellspacing','cellpadding','border')})
                    my_settings['attributes'].update({'img': ('src',)})
                    my_settings['separate'].add('tr')
                    my_settings['separate'].add('td')
                    my_settings['separate'].add('th')
                    sanitizer = html_sanitizer.Sanitizer(settings=my_settings)
                    content = sanitizer.sanitize(form.content.data)
                    print(content.encode('utf-8'))
                    article = mongo.db.articles.update_one({"_id":ObjectId(id)},{'$set':{"updated_at":datetime.now(),"thumbnail":thumbnail,"judul":form.judul.data,"category":ObjectId(form.category.data),"is_active":form.status.data.lower() in ('true'),"content":content.replace("B iosekuriti", "Biosekuriti").replace("d alam", "dalam")}})
                    flash({'Edit Article': 'Article update success!'}, 'success')
                    return redirect(url_for('admin_lumbungilmu.index'))
                else:
                    flash({'Edit Article': 'Article edit failed! Please contact admin.'}, 'danger')
                    return redirect(url_for('admin_lumbungilmu.edit',id=id))
            else:
                for x, y in form.errors.items():
                    form.errors[x] = y[0]
                flash(form.errors, 'danger')
                print(form.errors)
                return redirect(url_for('admin_lumbungilmu.edit',id=id))
    else:
        flash({'Edit Article': 'Article Unknown!.'}, 'danger')
        return redirect(url_for('admin_lumbungilmu.index'))

@admin_lumbungilmu.route('/delete/<string:id>', methods=["GET"])
@role_required(["admin"])
def delete(id):
    if ObjectId.is_valid(id):
        article = mongo.db.articles.find_one({"_id":ObjectId(id)})
        if article:
            # os.remove('application/static/img/articles/' + article["thumbnail"])
            mongo.db.statistics.remove({"article":ObjectId(id)})
            comments = mongo.db.comments.find({"article":ObjectId(id)})
            for x in comments:
                mongo.db.replies.remove({"comment_id":ObjectId(x["_id"])})
                mongo.db.comment.delete_one({"_id":ObjectId(x["_id"])})
            mongo.db.articles.delete_one({"_id":ObjectId(id)})
            flash({'Delete Article': 'Article deleted!'}, 'success')
            return redirect(url_for('admin_lumbungilmu.index'))
        else:
            flash({'Delete Article': 'Article not found!'}, 'danger')
            return redirect(url_for('admin_lumbungilmu.index'))
    else:
        flash({'Delete Article': 'Article Unknown!'}, 'danger')
        return redirect(url_for('admin_lumbungilmu.index'))

@admin_lumbungilmu.route('/header', methods=["GET","POST"])
@role_required(["admin"])
def header():
    if request.method == "POST":
        form = forms.BerasmerahHeaderForm()
        if form.validate_on_submit():
            if form.headerfile.data is not None:
                filename = secure_filename(form.headerfile.data.filename)
                for x in glob.glob('application/static/img/headers/lumbungilmu.*'):
                    os.remove(x)
                ext = filename.rsplit('.', 1)[1].lower()
                form.headerfile.data.save('application/static/img/headers/lumbungilmu.' + ext)
                flash({'Header': 'Change Header Successfull!'}, 'success')
                return redirect(url_for('admin_lumbungilmu.index'))
        else:
            for x, y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors, 'danger')
            return render_template('admin/lumbungilmu/header.html')
    else:
        return render_template('admin/lumbungilmu/header.html')