import os, re
from application import app, mongo, role_required
from flask import Blueprint, render_template, url_for, flash, redirect, request
from application.helpers import forms
from werkzeug import secure_filename
from operator import itemgetter

admin_home = Blueprint('admin_home', __name__, url_prefix='/admin/home')


@admin_home.route('/', methods=["GET"])
@role_required(["admin"])
def index():
    content1 = mongo.db.contents.find_one({"module":"home","element":"content1"})["data"]
    # content1["content_media"] = url_for('static', filename='img/home'+content1["content_media"])
    content2 = mongo.db.contents.find_one({"module":"home","element":"content2"})["data"]
    slider = mongo.db.contents.find_one({"module": "home", "element": "slider"})["data"]
    return render_template('admin/home.html', slider=slider, content1=content1, content2=content2)


@admin_home.route('/slider', methods=["GET"])
@role_required(["admin"])
def slider():
    return render_template('admin/home/slider.html')


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in app.config["SLIDER_ALLOWED_EXTENSION"]


@admin_home.route('/add_slider', methods=["POST"])
@role_required(["admin"])
def add_slider():
    form = forms.SliderForm()
    if form.validate_on_submit():
        slider = mongo.db.contents.find_one({"module": "home", "element": "slider"})["data"]
        if len(slider) != 0:
            sliderx = sorted(slider, key=itemgetter('order'), reverse=True)
            newslide = sliderx[0]
            newslide["order"] = str(int(newslide["order"]) + 1)
        else:
            newslide = {
                "status":True,
                "order":"1"
            }
        if allowed_file(form.slide.data.filename):
            filename = secure_filename(form.slide.data.filename)
            filename = filename.split('.')
            newslide["background_image"] = newslide["order"] +"."+ filename[1]
            form.slide.data.save('application/static/img/slider/'+newslide["background_image"])
            final = mongo.db.contents.find_one({"module": "home", "element": "slider"})["data"]
            final.append(newslide)
            mongo.db.contents.update_one({"module": "home", "element": "slider"}, {'$set': {"data": final}})
            flash({'Slider':'Slide sucessfully added.'},'success')
            return redirect(url_for('admin_home.index'))
    else:
        for x, y in form.errors.items():
            form.errors[x] = y[0]
        flash(form.errors, 'danger')
        return render_template('admin/home/slider.html')


@admin_home.route('/delete_slider/<string:urutan>', methods=["GET"])
@role_required(["admin"])
def delete_slider(urutan):
    slider = mongo.db.contents.find_one({"module": "home", "element": "slider"})["data"]
    if not any(x["order"] == urutan for x in slider):
        flash({'Slides': 'Slide tersebut tidak dapat ditemukan!.'}, 'danger')
        return redirect(url_for('admin_home.index'))
    else:
        for x in slider:
            if x["order"] == urutan:
                filename = x["background_image"]
        os.remove('application/static/img/slider/' + filename)
        newdata = [k for k in slider if k["order"] != urutan]
        mongo.db.contents.update_one({"module": "home", "element": "slider"}, {'$set': {"data": newdata}})
        return redirect(url_for('admin_home.index'))


@admin_home.route('/create', methods=["GET"])
@role_required(["admin"])
def create():
    return render_template('admin/home/create.html')


@admin_home.route('/edit', methods=["GET","POST"])
@role_required(["admin"])
def edit():
    if request.method == "POST":
        form = forms.HomeHeaderForm()
        if form.validate_on_submit():
            content1 = mongo.db.contents.find_one({"module": "home", "element": "content1"})["data"]
            content2 = mongo.db.contents.find_one({"module": "home", "element": "content2"})["data"]
            if form.content1file.data is not None:
                filename = secure_filename(form.content1file.data.filename)
                ext = filename.rsplit('.', 1)[1].lower()
                try:
                    os.remove('application/static/img/home/'+content1["content_media"])
                except:
                    pass
                form.content1file.data.save('application/static/img/home/content1.'+ext)
                content1["content_media"] = "content1."+ext
            content1["title"] = form.content1title.data
            content1["subtitle"] = form.content1subtitle.data
            videoUrl = form.content2link.data
            content2["title"] = form.content2title.data
            content2["content_media"] = re.sub(r"(?ism).*?=(.*?)$", r"https://www.youtube.com/embed/\1", videoUrl)
            content2["subtitle"] = form.content2subtitle.data
            mongo.db.contents.update_one({"module": "home", "element": "content1"}, {'$set': {"data": content1}})
            mongo.db.contents.update_one({"module": "home", "element": "content2"}, {'$set': {"data": content2}})
            flash({'Edit':'Edit Successfull!'},'success')
            return redirect(url_for('admin_home.index'))
        else:
            for x,y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors,'danger')
            return redirect(url_for('admin_home.edit'))
    else:
        content1 = mongo.db.contents.find_one({"module":"home","element":"content1"})["data"]
        content2 = mongo.db.contents.find_one({"module":"home","element":"content2"})["data"]
        return render_template('admin/home/edit.html', content1=content1, content2=content2)