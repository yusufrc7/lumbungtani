import os, glob
from bson.objectid import ObjectId
from application import app, mongo, role_required
from flask import Blueprint, render_template, url_for, request, flash, redirect
from application.helpers import forms
from werkzeug import secure_filename
from operator import itemgetter

admin_berasmerah = Blueprint('admin_berasmerah', __name__, url_prefix='/admin/berasmerah')

@admin_berasmerah.route('/', methods=["GET"])
@role_required(["admin"])
def index():
    products = mongo.db.contents.find_one({"module":"berasmerah", "element":"products"})["data"]
    return render_template('admin/berasmerah.html', products=products)

@admin_berasmerah.route('/create', methods=["GET", "POST"])
@role_required(["admin"])
def create():
    if request.method == "POST":
        form = forms.BerasmerahProductForm()
        if form.validate_on_submit():
            products = mongo.db.contents.find_one({"module": "berasmerah", "element": "products"})["data"]
            products_data = sorted(products, key=itemgetter('order'), reverse=True)
            try:
                neworder = str(int(products_data[0]["order"]) + 1)
            except IndexError:
                neworder = "1"
            if form.productpicture.data is not None:
                filename = secure_filename(form.productpicture.data.filename)
                ext = filename.rsplit('.', 1)[1].lower()
                form.productpicture.data.save('application/static/img/product/'+neworder+'.'+ext)
                products.append({"order" : neworder, "title" : form.productname.data, "subtitle" : form.productdesc.data, "background_image" : neworder+'.'+ext, "price" : float(form.productprice.data), "wa_number" : form.productWAnumber.data})
                mongo.db.contents.update_one({"module": "berasmerah", "element": "products"}, {'$set': {"data": products}})
                flash({'Add Product': 'Add Successfull!'}, 'success')
                return redirect(url_for('admin_berasmerah.index'))
        else:
            for x, y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors, 'danger')
            return render_template('admin/berasmerah/create.html')
    else:
        return render_template('admin/berasmerah/create.html')

@admin_berasmerah.route('/delete_product/<string:urutan>', methods=["GET"])
@role_required(["admin"])
def delete_product(urutan):
    products = mongo.db.contents.find_one({"module": "berasmerah", "element": "products"})["data"]
    if not any(x['order'] == urutan for x in products ):
        flash({'Product': 'Product tersebut tidak dapat ditemukan!.'}, 'danger')
        return redirect(url_for('admin_berasmerah.index'))
    else:
        for x in products:
            if x["order"] == urutan:
                filename = x["background_image"]
        os.remove('application/static/img/product/' + filename)
        newdata = [k for k in products if k["order"] != urutan]
        mongo.db.contents.update_one({"module": "berasmerah", "element": "products"}, {'$set': {"data": newdata}})
        flash({'Delete Product': 'Delete Successfull!'}, 'success')
        return redirect(url_for('admin_berasmerah.index'))


@admin_berasmerah.route('/edit/<int:id>', methods=["GET","POST"])
@role_required(["admin"])
def edit(id):
    if request.method == "POST":
        form = forms.BerasmerahProductEditForm()
        if form.validate_on_submit():
            products = mongo.db.contents.find_one({"module": "berasmerah", "element": "products"})["data"]
            product = next((item for item in products if item["order"] == str(id)), None)
            if form.productpicture.data is not None:
                filename = secure_filename(form.productpicture.data.filename)
                ext = filename.rsplit('.', 1)[1].lower()
                os.remove('application/static/img/product/'+product["background_image"])
                form.productpicture.data.save('application/static/img/product/'+product["order"]+'.'+ext)
                product["background_image"] = product["order"]+'.'+ext
            product["title"] = form.productname.data
            product["subtitle"] = form.productdesc.data
            product["price"] = float(form.productprice.data)
            product["wa_number"] = form.productWAnumber.data
            newproducts = [k for k in products if k["order"] != str(id)]
            newproducts.append(product)
            mongo.db.contents.update_one({"module": "berasmerah", "element": "products"},{'$set':{"data":newproducts}})
            flash({'Edit Product': 'Edit Successfull!'}, 'success')
            return redirect(url_for('admin_berasmerah.index'))
        else:
            for x,y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors,'danger')
            return redirect(url_for('admin_berasmerah.edit', id=str(id)))
    else:
        products = mongo.db.contents.find_one({"module": "berasmerah", "element": "products"})["data"]
        product = next((item for item in products if item["order"] == str(id)),None)
        if not product:
            flash({'Edit Product': 'Edit Failed, Unknown Product!'}, 'danger')
            return redirect(url_for('admin_berasmerah.index'))
        return render_template('admin/berasmerah/edit.html', product=product)

@admin_berasmerah.route('/header', methods=["GET", "POST"])
@role_required(["admin"])
def header():
    if request.method == "POST":
        form = forms.BerasmerahHeaderForm()
        if form.validate_on_submit():
            if form.headerfile.data is not None:
                filename = secure_filename(form.headerfile.data.filename)
                for x in glob.glob('application/static/img/headers/berasmerah.*'):
                    os.remove(x)
                ext = filename.rsplit('.', 1)[1].lower()
                form.headerfile.data.save('application/static/img/headers/berasmerah.'+ext)
                flash({'Header':'Change Header Successfull!'}, 'success')
                return redirect(url_for('admin_berasmerah.index'))
        else:
            for x,y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors,'danger')
            return redirect(url_for('admin_berasmerah.header'))
    else:
        return render_template('admin/berasmerah/header.html')