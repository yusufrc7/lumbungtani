import os, re
from bson.objectid import ObjectId
from application import app, mongo, role_required
from flask import Blueprint, render_template, url_for, request, flash, redirect
from flask_login import current_user
from application.helpers import forms


admin_pengguna = Blueprint('admin_pengguna', __name__, url_prefix='/admin/pengguna')

@admin_pengguna.route('/admin/list', methods=["GET","POST"])
@role_required(["admin"])
def admin(search=None):
    if request.method == "GET":
        admin = mongo.db.users.find({"role": "admin", "username": {"$ne": current_user.username}})
    else:
        search = str(request.form["search"])
        rgx = re.compile('.*'+search+'.*', re.IGNORECASE)
        admin = mongo.db.users.find({"role": "admin","$and":[{"username": {"$ne": current_user.username}},{"username":rgx}]})
    return render_template('admin/pengguna/admin/index.html', admins=admin, search=search)

@admin_pengguna.route('/visitor/list', methods=["GET","POST"])
@role_required(["admin"])
def visitor(search=None):
    if request.method == "GET":
        visitor = mongo.db.users.find({"role":"visitor"})
    else:
        search = str(request.form["search"])
        rgx = re.compile('.*'+search+'.*', re.IGNORECASE)
        visitor = mongo.db.users.find({"role":"visitor","username":rgx})
    return render_template('admin/pengguna/visitor/index.html', visitors=visitor, search=search)

@admin_pengguna.route('/admin/change_status', methods=["GET","POST"])
@role_required(["admin"])
def change_status():
    form = forms.StatusForm()
    if form.validate_on_submit():
        if ObjectId.is_valid(form.id.data):
            if form.status.data in ["true","True", True]:
                status = True
            elif form.status.data in ["False", "false", False]:
                status = False
            else:
                flash({'Status Change': 'Status Change Failed, Contact Technician!'}, 'danger')
                return redirect(url_for('admin_pengguna.admin'))
            mongo.db.users.update_one({"_id": ObjectId(form.id.data)}, {'$set': {"is_active":status}})
            flash({'Status Change': 'Status Change succesfull!'}, 'success')
            return redirect(url_for('admin_pengguna.admin'))
        else:
            flash({'Status Change': 'Status Change Failed, Admin Unknown!'}, 'danger')
            return redirect(url_for('admin_pengguna.admin'))
    else:
        flash({'Status Change': 'Status Change Failed, Contact Technician!'}, 'danger')
        return redirect(url_for('admin_pengguna.admin'))

@admin_pengguna.route('/visitor/change_status', methods=["GET","POST"])
@role_required(["admin"])
def v_change_status():
    form = forms.StatusForm()
    if form.validate_on_submit():
        if ObjectId.is_valid(form.id.data):
            if form.status.data in ["true","True", True]:
                status = True
            elif form.status.data in ["False", "false", False]:
                status = False
            else:
                flash({'Status Change': 'Status Change Failed, Contact Technician!'}, 'danger')
                return redirect(url_for('admin_pengguna.visitor'))
            mongo.db.users.update_one({"_id": ObjectId(form.id.data)}, {'$set': {"is_active":status}})
            flash({'Status Change': 'Status Change succesfull!'}, 'success')
            return redirect(url_for('admin_pengguna.visitor'))
        else:
            flash({'Status Change': 'Status Change Failed, User Unknown!'}, 'danger')
            return redirect(url_for('admin_pengguna.visitor'))
    else:
        flash({'Status Change': 'Status Change Failed, Contact Technician!'}, 'danger')
        return redirect(url_for('admin_pengguna.visitor'))

@admin_pengguna.route('/visitor/contrib_status', methods=["POST"])
@role_required(["admin"])
def contrib_status():
    form = forms.ContribForm()
    if form.validate_on_submit():
        if ObjectId.is_valid(form.id.data):
            if form.contrib.data in ["true","True", True]:
                contrib = True
            elif form.contrib.data in ["false","False", False]:
                contrib = False
            else:
                flash({'Status Change': 'Status Change Failed, Contact Technician!'}, 'danger')
                return redirect(url_for('admin_pengguna.visitor'))
            mongo.db.users.update_one({"_id": ObjectId(form.id.data)}, {'$set': {"is_contrib": contrib}})
            flash({'Status Change': 'Status Change succesfull!'}, 'success')
            return redirect(url_for('admin_pengguna.visitor'))
        else:
            flash({'Status Change': 'Status Change Failed, Admin Unknown!'}, 'danger')
            return redirect(url_for('admin_pengguna.visitor'))
    else:
        flash({'Status Change': 'Status Change Failed, Contact Technician!'}, 'danger')
        return redirect(url_for('admin_pengguna.visitor'))

@admin_pengguna.route('/admin/create', methods=["GET","POST"])
@role_required(["admin"])
def admin_create():
    if request.method == "POST":
        form = forms.AdminForm()
        if form.validate_on_submit():
            admin = mongo.db.users.find_one({"username":form.username.data})
            if admin is None:
                user = mongo.db.users.insert_one({"username" : str(form.username.data), "password" : str(form.password.data), "role" : "admin", "email" : str(form.email.data), "is_active" : True, "is_authenticated" : False})
                flash({'Add Admin':'Add Admin succesfull!'},'success')
                return redirect(url_for('admin_pengguna.admin'))
            else:
                flash({'username':'That username has already been taken.'},'danger')
            return redirect(url_for('admin_pengguna.admin_create'))
        else:
            for x, y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors, 'danger')
            print("WEW")
            return redirect(url_for('admin_pengguna.admin_create'))
    else:
        return render_template('admin/pengguna/admin/create.html')

@admin_pengguna.route('/admin/delete/<string:id>', methods=["GET"])
@role_required(["admin"])
def admin_delete(id):
    if ObjectId.is_valid(id):
        admin = mongo.db.users.find_one({"_id":ObjectId(id)})
        if admin:
            mongo.db.users.delete_one({"_id":ObjectId(id)})
            flash({'Delete Admin': 'Delete Admin succesfull!'}, 'success')
            return redirect(url_for('admin_pengguna.admin'))
        else:
            flash({'Delete Admin': 'Admin Unknown!'}, 'danger')
            return redirect(url_for('admin_pengguna.admin'))
    else:
        flash({'Delete Admin': 'ID Unknown!'}, 'danger')
        return redirect(url_for('admin_pengguna.admin'))

@admin_pengguna.route('/visitor/delete/<string:id>', methods=["GET"])
@role_required(["admin"])
def visitor_delete(id):
    if ObjectId.is_valid(id):
        visitor = mongo.db.users.find_one({"_id":ObjectId(id)})
        if visitor:
            mongo.db.users.delete_one({"_id":ObjectId(id)})
            flash({'Delete Visitor': 'Delete Visitor succesfull!'}, 'success')
            return redirect(url_for('admin_pengguna.visitor'))
        else:
            flash({'Delete User': 'User Unknown!'}, 'danger')
            return redirect(url_for('admin_pengguna.visitor'))
    else:
        flash({'Delete User': 'ID Unknown!'}, 'danger')
        return redirect(url_for('admin_pengguna.visitor'))