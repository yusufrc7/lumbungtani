import os, glob
from application import app, mongo, role_required
from flask import Blueprint, render_template, url_for, request, redirect, flash
from application.helpers import forms
from werkzeug import secure_filename

admin_pedati = Blueprint('admin_pedati', __name__, url_prefix='/admin/pedati')

@admin_pedati.route('/', methods=["GET"])
@role_required(["admin"])
def index():
    teams = mongo.db.contents.find_one({"module": "pedati", "element": "teams"})["data"]
    return render_template('admin/pedati.html', teams=teams)

@admin_pedati.route('/create', methods=["GET","POST"])
@role_required(["admin"])
def create():
    if request.method == "POST":
        form = forms.PedatiTeamForm()
        if form.validate_on_submit():
            teams = mongo.db.contents.find_one({"module": "pedati", "element": "teams"})["data"]
            if not any(x["email"] == form.email.data for x in teams):
                filename = form.avatar.data.filename
                ext = filename.rsplit('.', 1)[1].lower()
                social = []
                if form.facebook.data != "" and form.facebook.data != '' and form.facebook.data != None:
                    social.append({"type" : "facebook", "url" : form.facebook.data})
                if form.twitter.data != "" and form.twitter.data != '' and form.twitter.data != None:
                    social.append({"type" : "twitter", "url" : form.twitter.data})
                if form.instagram.data != "" and form.instagram.data != '' and form.instagram.data != None:
                    social.append({"type" : "instagram", "url" : form.instagram.data})
                if form.linkedin.data != "" and form.linkedin.data != '' and form.linkedin.data != None:
                    social.append({"type" : "linkedin", "url" : form.linkedin.data})
                basefilename = secure_filename(form.email.data)
                teams.append({"name" : form.name.data, "avatar_url" : str(basefilename)+'.'+ext, "desc" : form.bio.data, "role" : form.jabatan.data, "email" : form.email.data, "social" : social})
                form.avatar.data.save('application/static/img/team/'+str(basefilename)+'.'+ext)
                mongo.db.contents.update_one({"module": "pedati", "element": "teams"}, {'$set': {"data": teams}})
                flash({'Add Team': 'Add Successfull!'}, 'success')
                return redirect(url_for('admin_pedati.index'))
            else:
                flash({'New Team': 'Team with that email already exist!'}, 'danger')
                return render_template('admin/pedati/create.html')
        else:
            for x, y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors, 'danger')
            return render_template('admin/pedati/create.html')
    else:
        return render_template('admin/pedati/create.html')

@admin_pedati.route('/delete_team/<string:email>', methods=["GET"])
@role_required(["admin"])
def delete_team(email):
    teams = mongo.db.contents.find_one({"module": "pedati", "element": "teams"})["data"]
    if any(x["email"] == email for x in teams):
        for x in teams:
            if x["email"] == email:
                filename = x["avatar_url"]
        os.remove('application/static/img/team/' + filename)
        newdata = [k for k in teams if k["email"] != email]
        mongo.db.contents.update_one({"module": "pedati", "element": "teams"}, {'$set': {"data": newdata}})
        flash({'Delete Product': 'Delete Successfull!'}, 'success')
        return redirect(url_for('admin_pedati.index'))
    else:
        flash({'Delete Team': 'That team does not exist!'}, 'danger')
        return redirect(url_for('admin_pedati.index'))

@admin_pedati.route('/edit/<string:email>', methods=["GET","POST"])
@role_required(["admin"])
def edit(email):
    if request.method == "POST":
        form = forms.PedatiTeamEditForm()
        if form.validate_on_submit():
            teams = mongo.db.contents.find_one({"module": "pedati", "element": "teams"})["data"]
            team = next((item for item in teams if item["email"] == email), None)
            if form.avatar.data is not None:
                filename = secure_filename(form.avatar.data.filename)
                ext = filename.rsplit('.', 1)[1].lower()
                basefilename = secure_filename(form.email.data)
                os.remove('application/static/img/team/'+team["avatar_url"])
                form.avatar.data.save('application/static/img/team/'+str(basefilename)+'.'+ext)
                team["avatar_url"] = str(basefilename)+'.'+ext
            team["name"] = form.name.data
            team["desc"] = form.bio.data
            team["role"] = form.jabatan.data
            team["email"] = form.email.data
            social = []
            if form.facebook.data != "" and form.facebook.data != '' and form.facebook.data != None:
                social.append({"type" : "facebook", "url" : form.facebook.data})
            if form.twitter.data != "" and form.twitter.data != '' and form.twitter.data != None:
                social.append({"type" : "twitter", "url" : form.twitter.data})
            if form.instagram.data != "" and form.instagram.data != '' and form.instagram.data != None:
                social.append({"type" : "instagram", "url" : form.instagram.data})
            if form.linkedin.data != "" and form.linkedin.data != '' and form.linkedin.data != None:
                social.append({"type" : "linkedin", "url" : form.linkedin.data})
            team["social"] = social
            newteams = [k for k in teams if k["email"] != str(email)]
            newteams.append(team)
            mongo.db.contents.update_one({"module": "pedati", "element": "teams"},{'$set':{"data":newteams}})
            flash({'Change Data': 'Change Data Successfull!'}, 'success')
            return redirect(url_for('admin_pedati.index'))
        else:
            for x, y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors, 'danger')
            teams = mongo.db.contents.find_one({"module": "pedati", "element": "teams"})["data"]
            team = next((item for item in teams if item["email"] == email),None)
            facebook = next((item for item in team["social"] if item["type"] == "facebook"),{'type':'facebook','url':""})
            twitter = next((item for item in team["social"] if item["type"] == "twitter"),{'type':'twitter','url':""})
            instagram = next((item for item in team["social"] if item["type"] == "instagram"),{'type':'instagram','url':""})
            linkedin = next((item for item in team["social"] if item["type"] == "linkedin"),{'type':'linkedin','url':""})
            return render_template('admin/pedati/edit.html', team=team, facebook=facebook, twitter=twitter, instagram=instagram, linkedin=linkedin)
    else:
        teams = mongo.db.contents.find_one({"module": "pedati", "element": "teams"})["data"]
        team = next((item for item in teams if item["email"] == email),None)
        facebook = next((item for item in team["social"] if item["type"] == "facebook"),{'type':'facebook','url':""})
        twitter = next((item for item in team["social"] if item["type"] == "twitter"),{'type':'twitter','url':""})
        instagram = next((item for item in team["social"] if item["type"] == "instagram"),{'type':'instagram','url':""})
        linkedin = next((item for item in team["social"] if item["type"] == "linkedin"),{'type':'linkedin','url':""})
        return render_template('admin/pedati/edit.html', team=team, facebook=facebook, twitter=twitter, instagram=instagram, linkedin=linkedin)

@admin_pedati.route('/header', methods=["GET","POST"])
@role_required(["admin"])
def header():
    if request.method == "POST":
        form = forms.BerasmerahHeaderForm()
        if form.validate_on_submit():
            if form.headerfile.data is not None:
                filename = secure_filename(form.headerfile.data.filename)
                for x in glob.glob('application/static/img/headers/pedati.*'):
                    os.remove(x)
                ext = filename.rsplit('.', 1)[1].lower()
                form.headerfile.data.save('application/static/img/headers/pedati.'+ext)
                flash({'Header':'Change Header Successfull!'}, 'success')
                return redirect(url_for('admin_pedati.index'))
        else:
            for x,y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors,'danger')
            return redirect(url_for('admin_pedati.header'))
    else:
        return render_template('admin/pedati/header.html')