from flask_login import login_required
from application import app, mongo, role_required
from flask import Blueprint, render_template, url_for

admin_welcome = Blueprint('admin_welcome', __name__, url_prefix='/admin/welcome')

@admin_welcome.route('/', methods=["GET"])
@role_required(["admin","visitor"])
def index():
    return render_template('admin/welcome.html')