import os,glob, re
from bson.objectid import ObjectId
from application import app, mongo
from flask import Blueprint, render_template, url_for, request, current_app

pedati = Blueprint('pedati', __name__, url_prefix='/pedati')

@pedati.route('/', methods=["GET"])
def index():
    for x in glob.glob('application/static/img/headers/pedati.*'):
        header_image = url_for('static', filename=x)
    header_image = header_image.split('/')[-1:][0]
    header_image = url_for('static', filename="img/headers/"+header_image)
    header1 = mongo.db.contents.find_one({"module": "pedati", "element": "header1"})["data"]
    header2 = mongo.db.contents.find_one({"module": "pedati", "element": "header2"})["data"]
    teams = mongo.db.contents.find_one({"module": "pedati", "element": "teams"})["data"]
    for x in teams:
        x["avatar_url"] = url_for('static', filename="img/team/"+x["avatar_url"])
    visitor = mongo.db.users.find({"role":"visitor","is_contrib":True}).limit(4)
    contributors = []
    for x in visitor:
        detail = mongo.db.visitors.find_one({"_id":ObjectId(x["visitor_id"])})
        detail["avatar_url"] = url_for('static', filename="img/avatar/"+detail["avatar_url"])
        detail["email"] = x["email"]
        social = []
        if detail["facebook"] != "" and detail["facebook"] != '' and detail["facebook"] != None:
            social.append({"type": "facebook", "url": detail["facebook"]})
        if detail["twitter"] != "" and detail["twitter"] != '' and detail["twitter"] != None:
            social.append({"type": "twitter", "url": detail["twitter"]})
        if detail["instagram"] != "" and detail["instagram"] != '' and detail["instagram"] != None:
            social.append({"type": "instagram", "url": detail["instagram"]})
        if detail["linkedin"] != "" and detail["linkedin"] != '' and detail["linkedin"] != None:
            social.append({"type": "linkedin", "url": detail["linkedin"]})
        detail["social"] = social
        contributors.append(detail)
    current_app.logger.info(teams)
    return render_template('pedati.html', header1=header1, header2=header2, teams=teams, header_image=header_image, contributors=contributors)

@pedati.route('/contributors', methods=["GET","POST"])
def contributors():
    if request.method == "GET":
        visitor = mongo.db.users.find({"role":"visitor","is_contrib":True})
    else:
        search = str(request.form["search"])
        rgx = re.compile('.*' + search + '.*', re.IGNORECASE)
        visitor = mongo.db.users.find({"role": "visitor", "is_contrib": True,"username":rgx})
    for x in glob.glob('application/static/img/headers/pedati.*'):
        header_image = url_for('static', filename=x)
    header_image = header_image.split('/')[-1:][0]
    header_image = url_for('static', filename="img/headers/"+header_image)
    contributors = []
    for x in visitor:
        detail = mongo.db.visitors.find_one({"_id":ObjectId(x["visitor_id"])})
        detail["avatar_url"] = url_for('static', filename="img/avatar/"+detail["avatar_url"])
        detail["email"] = x["email"]
        print(detail)
        social = []
        if detail["facebook"] != "" and detail["facebook"] != '' and detail["facebook"] != None:
            social.append({"type": "facebook", "url": detail["facebook"]})
        if detail["twitter"] != "" and detail["twitter"] != '' and detail["twitter"] != None:
            social.append({"type": "twitter", "url": detail["twitter"]})
        if detail["instagram"] != "" and detail["instagram"] != '' and detail["instagram"] != None:
            social.append({"type": "instagram", "url": detail["instagram"]})
        if detail["linkedin"] != "" and detail["linkedin"] != '' and detail["linkedin"] != None:
            social.append({"type": "linkedin", "url": detail["linkedin"]})
        detail["social"] = social
        contributors.append(detail)
    print(contributors)
    return render_template('contributors.html', header_image=header_image, contributors=contributors)