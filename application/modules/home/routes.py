from application import app, mongo, mail
from application.helpers import forms
from flask import Blueprint, render_template, url_for, request, jsonify, redirect, flash, send_file
from flask_login import login_user, current_user, logout_user
from application.models.users import User
from flask_mail import Message

home = Blueprint('home', __name__)

@home.route('/.well-known/pki-validation/92CD5FC72D1082961E9310C22A11C894.txt')
def ssl():
    return send_file('modules/home/ssl.txt')

@home.route('/', methods=["GET"])
def index():
    header = mongo.db.contents.find_one({"module":"home","element":"header"})["data"]
    content1 = mongo.db.contents.find_one({"module":"home","element":"content1"})["data"]
    content2 = mongo.db.contents.find_one({"module":"home","element":"content2"})["data"]
    slider = mongo.db.contents.find_one({"module":"home","element":"slider"})["data"]
    slider = [x for x in slider if x["status"]]
    for x in slider:
        x["background_image"] = url_for('static', filename='img/slider/'+x["background_image"])
    content1["content_media"] = url_for('static', filename='img/home/'+content1["content_media"])
    return render_template('index.html', header=header,content1=content1,content2=content2, slider=slider)

@home.route('/email1', methods=["GET"])
def email1():
    return render_template('email1.html')

@home.route('/email2', methods=["GET"])
def email2():
    return render_template('email2.html')

@home.route('/login', methods=["GET","POST"])
def login():
    if request.method == "POST":
        form = forms.LoginForm()
        if form.validate_on_submit():
            user = mongo.db.users.find_one({'username':str(form.username.data),'password':str(form.password.data)})
            if user:
                user = User(username=str(form.username.data))
                login_user(user)
                return redirect(url_for('admin_welcome.index'))
            else:
                flash({'User':'Maaf, Username atau Password anda Salah!.'},'danger')
                return render_template('login.html')
        else:
            for x,y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors,'danger')
            return render_template('login.html')
    else:
        if current_user.is_anonymous:
            pass
        else:
            return redirect(url_for('admin_welcome.index'))
        return render_template('login.html')

@home.route('/register', methods=["GET","POST"])
def register():
    if request.method == 'POST':
        form = forms.RegisterForm()
        if form.validate_on_submit():
            users = mongo.db.users.find_one({'username':str(form.username.data)})
            if users is None:
                visitor = mongo.db.visitors.insert_one({"fullname" : "", "phone" : "", "avatar_url" : "user_default.png", "facebook" : "", "twitter" : "", "instagram" : "", "linkedin" : "", "jabatan" : "", "bio" : ""})
                user = mongo.db.users.insert_one({"visitor_id":visitor.inserted_id,"username" : str(form.username.data), "password" : str(form.password.data), "role" : 'visitor', "email" : str(form.email.data), "is_active" : True, "is_authenticated" : False,"is_contrib":False})
                if str(form.role.data) == 'contributor':
                    try:
                        msg = Message(str(form.email.data) + " meminta menjadi Kontributor",
                                      html=render_template('email1.html', name=str(form.username.data), uname=str(form.username.data)),
                                      sender="lumbungtani.indonesia@gmail.com",
                                      recipients=["lumbungtani.indonesia@gmail.com"])
                        mail.send(msg)
                    except:
                        pass
                flash({'register':'Register Successfull! You can start login'},'success')
                return redirect(url_for('home.index'))
            else:
                flash({'username':'That username has already been taken.'},'danger')
                return render_template('register.html')
        else:
            for x,y in form.errors.items():
                form.errors[x] = y[0]
            flash(form.errors,'danger')
            return render_template('register.html')
    else:
        return render_template('register.html')

@home.route('/hubungi', methods=["POST"])
def hubungi():
    form = forms.ContactForm()
    if form.validate_on_submit():
        msg = Message(str(form.email.data)+" mengirimkan kontak kepada Anda",html=render_template('email2.html', name=str(form.name.data), email=str(form.email.data),subject=str(form.subject.data)), sender="lumbungtani.indonesia@gmail.com", recipients=["lumbungtani.indonesia@gmail.com"])
        mail.send(msg)
        flash("Mail Sent!", 'success')
        return redirect(url_for('home.index'))
    else:
        for x, y in form.errors.items():
            form.errors[x] = y[0]
        flash(form.errors, 'danger')
        return redirect(url_for('home.index'))

@home.route('/logout', methods=["GET"])
def logout():
    logout_user()
    return redirect(url_for('home.index'))