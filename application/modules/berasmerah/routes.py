import os,glob
from application import app, mongo
from flask import Blueprint, render_template, url_for

berasmerah = Blueprint('berasmerah', __name__, url_prefix='/berasmerah')

@berasmerah.route('/', methods=["GET"])
def index():
    for x in glob.glob('application/static/img/headers/berasmerah.*'):
        header_image = url_for('static', filename=x)
    header_image = header_image.split('/')[-1:][0]
    header_image = url_for('static', filename="img/headers/"+header_image)
    header = mongo.db.contents.find_one({"module": "berasmerah", "element": "header"})["data"]
    products = mongo.db.contents.find_one({"module":"berasmerah", "element":"products"})["data"]
    for x in products:
        x["background_image"] = url_for('static', filename='img/product/'+x["background_image"])
    # products = {
    #     "title": "Judul",
    #     "subtitle": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    #     "panels": [
    #         {
    #             "title": "Judul 1",
    #             "subtitle": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    #             "background_image": "1.jpg"
    #         }, {
    #             "title": "Judul 2",
    #             "subtitle": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    #             "background_image": "2.jpg"
    #         }, {
    #             "title": "Judul 3",
    #             "subtitle": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    #             "background_image": "3.jpg"
    #         }, {
    #             "title": "Judul 4",
    #             "subtitle": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    #             "background_image": "4.jpg"
    #         }
    #     ]
    # }
    return render_template('berasmerah.html', header=header, products=products, header_image=header_image)

