from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SelectField, RadioField, TextAreaField
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms.validators import DataRequired

class RegisterForm(FlaskForm):
    username = StringField('name',validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    email = PasswordField('password', validators=[DataRequired()])
    role = SelectField('role', choices=[('visitor','Visitor'),('contributor','Kontributor')])

class LoginForm(FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])

class SliderForm(FlaskForm):
    slide = FileField('slide', validators=[FileRequired(),FileAllowed(['jpg','jpeg','png'], 'Hanya mendukung format JPG, JPEG dan PNG')])

class HomeHeaderForm(FlaskForm):
    content1title = StringField('content1title', validators=[DataRequired()])
    content1file = FileField('content1file', validators=[FileAllowed(['jpg','jpeg','png'], 'Hanya mendukung format JPG, JPEG dan PNG')])
    content1subtitle = StringField('content1subtitle', validators=[DataRequired()])
    content2title = StringField('content2title', validators=[DataRequired()])
    content2link = StringField('content2link', validators=[DataRequired()])
    content2subtitle = StringField('content2subtitle', validators=[DataRequired()])

class BerasmerahProductForm(FlaskForm):
    productname = StringField('productname', validators=[DataRequired()])
    productprice = StringField('productprice', validators=[DataRequired()])
    productpicture = FileField('productpicture', validators=[FileRequired(),FileAllowed(['jpg','jpeg','png'], 'Hanya mendukung format JPG, JPEG dan PNG')])
    productWAnumber = StringField('productWAnumber', validators=[DataRequired()])
    productdesc = StringField('productdesc', validators=[DataRequired()])

class BerasmerahProductEditForm(FlaskForm):
    productname = StringField('productname', validators=[DataRequired()])
    productprice = StringField('productprice', validators=[DataRequired()])
    productpicture = FileField('productpicture', validators=[FileAllowed(['jpg','jpeg','png'], 'Hanya mendukung format JPG, JPEG dan PNG')])
    productWAnumber = StringField('productWAnumber', validators=[DataRequired()])
    productdesc = StringField('productdesc', validators=[DataRequired()])

class BerasmerahHeaderForm(FlaskForm):
    headerfile = FileField('productpicture', validators=[FileRequired(),FileAllowed(['jpg','jpeg','png'], 'Hanya mendukung format JPG, JPEG dan PNG')])

class PedatiTeamForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    jabatan = StringField('jabatan', validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired()])
    avatar = FileField('avatar', validators=[FileRequired(),FileAllowed(['jpg','jpeg','png'], 'Hanya mendukung format JPG, JPEG dan PNG')])
    facebook = StringField('facebook')
    twitter = StringField('twitter')
    instagram = StringField('instagram')
    linkedin = StringField('linkedin')
    bio = StringField('bio', validators=[DataRequired()])

class PedatiTeamEditForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    jabatan = StringField('jabatan', validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired()])
    avatar = FileField('avatar', validators=[FileAllowed(['jpg','jpeg','png'], 'Hanya mendukung format JPG, JPEG dan PNG')])
    facebook = StringField('facebook')
    twitter = StringField('twitter')
    instagram = StringField('instagram')
    linkedin = StringField('linkedin')
    bio = StringField('bio', validators=[DataRequired()])

class AdminForm(FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    fullname = StringField('fullname', validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired()])

class StatusForm(FlaskForm):
    id = StringField('id', validators=[DataRequired()])
    status = SelectField('status', choices=[('true',True),('false',False)])

class ContribForm(FlaskForm):
    id = StringField('id', validators=[DataRequired()])
    contrib = RadioField('contrib', choices=[('true','true'),('false','false')])

class ContactForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired()])
    subject = StringField('subject', validators=[DataRequired()])

class ArticleForm(FlaskForm):
    judul = StringField('name', validators=[DataRequired()])
    thumbnail = FileField('thumbnail', validators=[FileRequired(),FileAllowed(['jpg','jpeg','png'], 'Hanya mendukung format JPG, JPEG dan PNG')])
    category = StringField('category')
    status = SelectField('status', choices=[('true','true'),('false','false')])
    content = TextAreaField('content', validators=[DataRequired()])
    infographic = FileField('infographic', validators=[FileRequired(),FileAllowed(['pdf'], 'Hanya mendukung format PDF')])

class ArticleEditForm(FlaskForm):
    judul = StringField('name', validators=[DataRequired()])
    thumbnail = FileField('thumbnail', validators=[FileAllowed(['jpg','jpeg','png'], 'Hanya mendukung format JPG, JPEG dan PNG')])
    category = StringField('category', validators=[DataRequired()])
    status = SelectField('status', choices=[('true','true'),('false','false')])
    content = TextAreaField('content', validators=[DataRequired()])

class CommentForm(FlaskForm):
    comment = TextAreaField('comment', validators=[DataRequired()])

class EditProfileForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    avatar = FileField('avatar', validators=[FileAllowed(['jpg','jpeg','png'], 'Hanya mendukung format JPG, JPEG dan PNG')])
    phone = StringField('phone')
    facebook = StringField('facebook')
    twitter = StringField('twitter')
    instagram = StringField('instagram')
    linkedin = StringField('linkedin')
    bio = StringField('bio')