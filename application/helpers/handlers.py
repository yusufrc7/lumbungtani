from application import login_manager
from flask import render_template
from flask_login import current_user

@login_manager.unauthorized_handler
def unauthorized():
    return render_template('error_handler/401.html')