from bson.objectid import ObjectId
from application import app,mongo,login_manager
from werkzeug.security import generate_password_hash, check_password_hash
from flask import url_for
from flask_login import UserMixin

class User(UserMixin):
    def __init__(self, username):
        user = mongo.db.users.find_one({'username': username})
        self.username = user["username"]
        self.role = user["role"]
        self.email = user["email"]
        self.id = user["_id"]
        try:
            self.is_contrib = user["is_contrib"]
        except:
            self.is_contrib = False
        if user["role"] != "admin":
            detail = mongo.db.visitors.find_one({"_id": ObjectId(user["visitor_id"])})
            self.visitor_id = user["visitor_id"]
            self.phone = detail["phone"]
            self.fullname = detail["fullname"]
            self.avatar_url = url_for('static', filename="img/avatar/" + detail["avatar_url"])
            self.facebook = detail["facebook"]
            self.twitter = detail["twitter"]
            self.instagram = detail["instagram"]
            self.linkedin = detail["linkedin"]
            self.bio = detail["bio"]
        else:
            self.fullname = "Admin"
            self.avatar_url = url_for('static', filename="img/notfound.png")
        self.is_active = user["is_active"]
        self.is_authenticated = True

    def is_authenticated(self):
        return self.is_authenticated

    def is_active(self):
        return self.is_active

    def get_id(self):
        return self.username

    def encode(self):
        return self.__dict__

@login_manager.user_loader
def load_user(username):
    user = mongo.db.users.find_one({'username': username})
    if not user:
        return None
    return User(username=user['username'])